package com.pacevitch.client;

import com.pacevitch.client.repo.Client;
import com.pacevitch.client.service.ClientGreeting;
import com.pacevitch.client.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("clients")
@RequiredArgsConstructor
public class ClientController {

    final ClientService clientService;

    @GetMapping(value = "", produces = { "application/hal+json" })
    public @ResponseBody
    CollectionModel<Client> getClients() {
        List<Client> clients = clientService.getClients();
        clients.stream().forEach(client -> client.add(linkTo(ClientController.class).slash(client.getId()).withSelfRel()));
        return CollectionModel.of(clients, linkTo(ClientController.class).withSelfRel());
    }

    @GetMapping(value = "/{id}", produces = { "application/hal+json" })
    public @ResponseBody
    HttpEntity<Client> getClient(@PathVariable Long id) {
        Client client = clientService.getClient(id);
        client.add(linkTo(methodOn(ClientController.class).getClient(id)).withSelfRel());
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/greeting", produces = { "application/hal+json" })
    public @ResponseBody
    HttpEntity<ClientGreeting> getClientGreeting(@PathVariable Long id) {
        ClientGreeting greeting = clientService.getClientGreeting(id);
        greeting.add(linkTo(methodOn(ClientController.class).getClientGreeting(id)).withSelfRel());
        return new ResponseEntity<>(greeting, HttpStatus.OK);
    }

}

package com.pacevitch.client.config;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "com.pacevitch.client.repo")
@EnableEncryptableProperties
public class ApplicationConfiguration {
}

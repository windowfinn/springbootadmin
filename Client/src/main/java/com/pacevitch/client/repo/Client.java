package com.pacevitch.client.repo;

import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/*
https://stackoverflow.com/questions/34241718/lombok-builder-and-jpa-default-constructor
 */
@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
public class Client extends RepresentationModel<Client> implements Serializable {

    /*
     * IMPORTANT:
     * Set toString, equals, and hashCode as described in these
     * documents:
     * - https://vladmihalcea.com/the-best-way-to-implement-equals-hashcode-and-tostring-with-jpa-and-hibernate/
     * - https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
     * - https://vladmihalcea.com/hibernate-facts-equals-and-hashcode/
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;

    private String lastName;
    private String career;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Client))
            return false;

        Client other = (Client) o;

        return id != null &&
                id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}

package com.pacevitch.client.service;

import lombok.Builder;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

@Data
@Builder
public class ClientGreeting extends RepresentationModel<ClientGreeting> {
    private final String greeting;
}

package com.pacevitch.client.service;

import com.pacevitch.client.repo.Client;
import com.pacevitch.client.repo.ClientRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RefreshScope //Required to allow bean to be refreshed
public class ClientDatabaseService implements ClientService {

    private final String greeting;
    private final ClientRepository clientRepository;

    //@Autowire is not required as only one constructor
    public ClientDatabaseService(@Value("${client.greeting}") String greeting, ClientRepository clientRepository) {
        this.greeting = greeting;
        this.clientRepository = clientRepository;
    }

    @Override
    public Client getClient(Long id) {
        return clientRepository.findById(id).orElseThrow();
    }

    @Override
    public List<Client> getClients() {
        List<Client> clients = new ArrayList<>();
        clientRepository.findAll().forEach(clients::add);
        return clients;
    }

    @Override
    public ClientGreeting getClientGreeting(Long id) {
        return ClientGreeting.builder().greeting(String.format(greeting, getClient(id).getCareer().substring(12))).build();
    }
}

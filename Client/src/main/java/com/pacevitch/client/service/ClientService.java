package com.pacevitch.client.service;

import com.pacevitch.client.repo.Client;

import java.util.List;

public interface ClientService {
    Client getClient(Long id);
    List<Client> getClients();
    ClientGreeting getClientGreeting(Long id);
}

package com.pacevitch.client;

import com.pacevitch.client.repo.Client;
import com.pacevitch.client.service.ClientService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ClientController.class)
class ClientControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    ClientService clientService;

    private Long clientId = 1l;
    private Client clientOne;

    @BeforeEach
    public void setup(){
        clientOne = Client.builder().id(clientId).firstName("firstName").lastName("lastname").career("Money Salesman").build();
    }

    @Test
    @SneakyThrows
    @WithMockUser(value = "spring")
    public void getClientsShouldReturnClientList() {
        when(clientService.getClients()).thenReturn(Arrays.asList(clientOne));
        mvc.perform(get("/clients"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(jsonPath("$._embedded.clientList.length()").value(1));
        Mockito.verify(clientService).getClients();
    }

    @Test
    @SneakyThrows
    @WithMockUser(value = "spring")
    public void getClientShouldReturnNotFound() {
        //Cant test for 404 as the appropriate spring handlers aren't in place
        when(clientService.getClient(clientId)).thenThrow(new NoSuchElementException());

        assertThatThrownBy(
                        () -> mvc.perform(get("/clients/{clientId}", clientId).contentType(MediaType.APPLICATION_JSON)))
                .hasCauseInstanceOf(NoSuchElementException.class);
    }

    @Test
    @SneakyThrows
    @WithMockUser(value = "spring")
    public void getClientShouldReturnAClient() {
        when(clientService.getClient(clientId)).thenReturn(clientOne);
        mvc.perform(get("/clients/{clientId}", clientId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(jsonPath("$.firstName").value("firstName"));
        Mockito.verify(clientService).getClient(clientId);
    }

    @Test
    @SneakyThrows
    public void getClientsIsSecure() {
        mvc.perform(get("/clients", clientId))
                .andDo(print())
                .andExpect(status().isUnauthorized());
        Mockito.verify(clientService, times(0)).getClients();
    }

    @Test
    @SneakyThrows
    public void getClientIsSecure() {
        mvc.perform(get("/clients/{clientId}", clientId))
                .andDo(print())
                .andExpect(status().isUnauthorized());
        Mockito.verify(clientService, times(0)).getClient(clientId);
    }

}
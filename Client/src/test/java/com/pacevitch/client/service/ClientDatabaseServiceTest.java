package com.pacevitch.client.service;

import com.pacevitch.client.repo.Client;
import com.pacevitch.client.repo.ClientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class ClientDatabaseServiceTest {

    @MockBean
    private ClientRepository repository;

    private ClientDatabaseService service;
    private String greeting = "Hello %s";
    private Long clientId = 1l;
    private Client clientOne;

    @BeforeEach
    public void setup(){
        clientOne = Client.builder().id(clientId).firstName("").lastName("").career("Money Salesman").build();
        service = new ClientDatabaseService(greeting, repository);
    }

    @Test
    public void hasRefreshScopeAnnotation() {
        assertTrue(ClientDatabaseService.class.isAnnotationPresent(RefreshScope.class));
    }

    @Test
    public void getClient_ThrowsExceptionWhenClientNotFound() {
        when(repository.findById(clientId)).thenThrow(new NoSuchElementException());

        assertThrows(NoSuchElementException.class, () -> service.getClient(clientId));
    }

    @Test
    public void getClient_CallsRepositoryWithClientId() {
        when(repository.findById(clientId)).thenReturn(Optional.of(clientOne));
        service.getClient(clientId);
        verify(repository).findById(ArgumentMatchers.eq(clientId));
    }

    @Test
    public void getClients_CallsRepositoryFindAll() {
        Mockito.when(repository.findAll()).thenReturn(Arrays.asList(clientOne));
        service.getClients();
        verify(repository).findAll();
    }

    @Test
    public void getClientGreeting_CallsRepositoryWithClientIdAndGreetingUsesCareer(){
        when(repository.findById(clientId)).thenReturn(Optional.of(clientOne));
        ClientGreeting greeting = service.getClientGreeting(clientId);
        assertNotNull(greeting);
        assertEquals(String.format(greeting.getGreeting(), clientOne.getCareer()), greeting.getGreeting());
        verify(repository).findById(ArgumentMatchers.eq(clientId));
    }

}
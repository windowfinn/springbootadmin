# SpringBootAdmin Server and Client

A multi-project Gradle build consisting of a SpringBootAdmin server 
and a simple SpringBoot application acting as the client.

## Running

``./gradlew clean dockerTag dockerComposeUp``

##Using

Visit:

``http://localhost:18888``

then login to the Admin Server UI with admin/admin.

The client should be registered and shown as up.

Drill down to the details.

In the Environment section you can amend properties in the running application

[Provided the project imports spring-cloud-starter and relevant class had the
@RefreshScope annotation]

The client has a secure endpoint [login: client/client] which returns client info from an in memory database.

```
http://localhost:19999/clients
http://localhost:19999/clients/{id}
http://localhost:19999/clients/{id}/greeting
```

#### Amend a property
The greeting endpoint returns a formatted string based on the clients career.

i.e. 

```
{
    "greeting": "Hello Industrialist"
}
```

The greeting is a property that has been injected into a bean.

As the class which uses it has ```@RefreshScope``` annotation it can be amended
in the Environment section.

The property is ```client.greeting```.

Enter the key and a new value (i.e. "Goodbye %s"), Update, and Refresh Context.

Call the greeting endpoint once again and the message will have changed.

## Stopping

``./gradlew dockerComposeDown``

## [Jasypt](http://www.jasypt.org/download.html)

./encrypt.sh input=YYYYYYYYY password=XXXXXXXXXXXXXXXX algorithm=PBEWITHHMACSHA512ANDAES_256 ivGeneratorClassName=org.jasypt.iv.RandomIvGenerator




